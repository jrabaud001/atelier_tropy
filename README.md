# Atelier_Tropy

## [Tropy](https://tropy.org/), canaux officiels :
- [Documentation](https://docs.tropy.org/)
- [Blog](https://tropy.org/blog/)
  - 2023-03-31 : [New Project Types in Tropy 1.13](https://tropy.org/blog/new-project-types-in-tropy-1-13)
- [Support](https://forums.tropy.org/) (forum)
- [Vimeo](https://vimeo.com/user73164761)
- [Youtube](https://www.youtube.com/watch?v=jWjP90EWHkQ&feature=youtu.be)
- [Twitter](https://twitter.com/tropy)
- [GitHub](https://github.com/tropy)

## Extensions

| Nom + lien | Description |
| --- | --- |
| [tropy-plugin-csv](https://github.com/tropy/tropy-plugin-csv) | Tropy plugin to import items from a CSV file, and export your items to CSV. |
| [tropy-plugin-omeka](https://github.com/tropy/tropy-plugin-omeka) | Tropy plugin to export selected items into an [Omeka S](https://omeka.org/s/) instance. |
| [tropy-plugin-archive](https://github.com/tropy/tropy-plugin-archive) | Tropy plugin for exporting items into a single zip archive. This includes all the metadata, as well as the photo files. |
| [tropy-plugin-csl](https://github.com/tropy/tropy-plugin-csl) | Tropy plugin to export your items to Zotero as CSL/JSON. |
| [tropy-plugin-iiif](https://github.com/tropy/tropy-plugin-iiif) | Tropy plugin to import IIIF manifests. |



## Tutoriels

- [George Mason University Library](https://infoguides.gmu.edu/tropy/introduction) (where the *RRCHNM* is.)
- Université de Lille | Pole-Num-Scrums-Skills - [Tropy | gestion d'images](https://wikis.univ-lille.fr/proj-polnum/accueil/manuels/guide-d-utilisation-de-tropy)
- BULAC - [Support de formation - Tropy](https://www.bulac.fr/document/support-de-formation-tropy-2022-04) 2022-04
- Rennes 2 - [Gérer ses photos de recherche avec Tropy](https://tutos.bu.univ-rennes2.fr/c.php?g=702342)
- [Handout - Tropy and Archival Fieldwork (2 p.)](https://yorkspace.library.yorku.ca/xmlui/bitstream/handle/10315/36607/Handout-Tropy%20and%20Archival%20Fieldwork.pdf?sequence=3&isAllowed=y)
- [Schlesinger Library on the History of Women in America (*Radcliffe Institute for Advanced Study*) - Harvard University](https://guides.library.harvard.edu/c.php?g=833532&p=5990005)


### Video 

- [Tropy Webinar](https://www.youtube.com/watch?v=jWjP90EWHkQ&t=2701s), June 16, 2020 (unedited recording) - \[par [Abby Mullen](https://abbymullen.org)\]


## Billets de blog

- [Gérer ses photos d’archives avec Tropy](http://www.boiteaoutils.info/2017/10/gerer-ses-photos-darchives-avec-tropy/) - Franziska Heimburger - *La boîte à outils des historien·ne·s* (2017)
- [Tropy, un gestionnaire de photos d'archives pour les chercheurs](https://www.macg.co/logiciels/2017/10/tropy-un-gestionnaire-de-photos-darchives-pour-les-chercheurs-100197) - Florian Innocente, *MacGeneration* (2017)
- [Six months of using Tropy](https://www.e-mourlon-druol.com/six-months-of-using-tropy/) - Emmanuel Mourlon-Druol (2019)
- [Tropy : un logiciel pour organiser des corpus iconographiques](https://bulac.hypotheses.org/33406) - BULAC (2021)

## Bonus

### [Linked Open Vocabularies (LOV)](https://lov.linkeddata.es/dataset/lov/)

- Exemple : [Europeana Data Model vocabulary (edm)](https://lov.linkeddata.es/dataset/lov/vocabs/edm)

- [Getty Vocabularies as Linked Open Data](https://www.getty.edu/research/tools/vocabularies/lod/index.html)
- CIDOC-CRM  (archéologie, musées) : [Scope](https://www.cidoc-crm.org/scope)

#### DublinCore

Site officiel :
- [Dublin Core™ User Guide](https://www.dublincore.org/resources/userguide/)
- [DCMI Metadata Terms](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/)


<details>
    <summary><strong>Properties</strong> in the <code>/elements/1.1/</code> namespace: </summary>

- [contributor](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/contributor)
- [coverage](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/coverage)
- [creator](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/creator)
- [date](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/date)
- [description](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/description)
- [format](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/format)
- [identifier](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/identifier)
- [language](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/language)
- [publisher](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/publisher)
- [relation](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/relation)
- [rights](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/rights)
- [source](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/source)
- [subject](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/subject)
- [title](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/title)
- [type](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/elements/1.1/type)

</details>

<details>
    <summary><strong>Properties</strong> in the <code>/terms/</code> namespace: </summary>

- [abstract](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/abstract)
- [accessRights](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/accessRights)
- [accrualMethod](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/accrualMethod)
- [accrualPeriodicity](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/accrualPeriodicity)
- [accrualPolicy](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/accrualPolicy)
- [alternative](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/alternative)
- [audience](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/audience)
- [available](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/available)
- [bibliographicCitation](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/bibliographicCitation)
- [conformsTo](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/conformsTo)
- [contributor](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/contributor)
- [coverage](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/coverage)
- [created](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/created)
- [creator](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/creator)
- [date](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/date)
- [dateAccepted](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/dateAccepted)
- [dateCopyrighted](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/dateCopyrighted)
- [dateSubmitted](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/dateSubmitted)
- [description](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/description)
- [educationLevel](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/educationLevel)
- [extent](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/extent)
- [format](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/format)
- [hasFormat](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/hasFormat)
- [hasPart](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/hasPart)
- [hasVersion](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/hasVersion)
- [identifier](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/identifier)
- [instructionalMethod](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/instructionalMethod)
- [isFormatOf](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/isFormatOf)
- [isPartOf](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/isPartOf)
- [isReferencedBy](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/isReferencedBy)
- [isReplacedBy](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/isReplacedBy)
- [isRequiredBy](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/isRequiredBy)
- [issued](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/issued)
- [isVersionOf](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/isVersionOf)
- [language](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/language)
- [license](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/license)
- [mediator](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/mediator)
- [medium](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/medium)
- [modified](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/modified)
- [provenance](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/provenance)
- [publisher](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/publisher)
- [references](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/references)
- [relation](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/relation)
- [replaces](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/replaces)
- [requires](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/requires)
- [rights](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/rights)
- [rightsHolder](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/rightsHolder)
- [source](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/source)
- [spatial](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/spatial)
- [subject](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/subject)
- [tableOfContents](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/tableOfContents)
- [temporal](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/temporal)
- [title](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/title)
- [type](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/type)
- [valid](https://www.dublincore.org/specifications/dublin-core/dcmi-terms/#http://purl.org/dc/terms/valid) 

</details>

### IIIF

- Site officiel : https://iiif.io
- Chez Biblissima : 
  - [Introduction à IIIF](https://doc.biblissima.fr/iiif/introduction-iiif/)
  - [Formation IIIF](https://doc.biblissima.fr/formation-iiif/)